package com.devcamp.shoppizzacrud.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shoppizzacrud.model.ProductLine;
import com.devcamp.shoppizzacrud.service.ProductLineService;

@CrossOrigin
@RestController
@RequestMapping("/product-lines")
public class ProductLineController {
    @Autowired
    private ProductLineService productLineService;

    @GetMapping
    public ResponseEntity<List<ProductLine>> getAllProductLines() {
        try {
            List<ProductLine> productLines = productLineService.getAllProductLines();
            return new ResponseEntity<>(productLines, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductLine> getProductLineById(@PathVariable("id") int id) {
        try {
            Optional<ProductLine> productLineData = productLineService.getProductLineById(id);
            if (productLineData.isPresent()) {
                return new ResponseEntity<>(productLineData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<Object> createProductLine(@Valid @RequestBody ProductLine paramProductLine) {
        return productLineService.createProductLine(paramProductLine);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateProductLine(@PathVariable Integer id,
            @Valid @RequestBody ProductLine paramProductLine) {
        return productLineService.updateProductLine(id, paramProductLine);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Object> deleteProductLineById(@PathVariable Integer id) {
        return productLineService.deleteProductLineById(id);
    }
}
