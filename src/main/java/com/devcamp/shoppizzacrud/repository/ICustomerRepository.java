package com.devcamp.shoppizzacrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shoppizzacrud.model.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {

}
