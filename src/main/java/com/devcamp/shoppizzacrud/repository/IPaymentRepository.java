package com.devcamp.shoppizzacrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shoppizzacrud.model.Payment;

public interface IPaymentRepository extends JpaRepository<Payment, Integer> {
    
}
