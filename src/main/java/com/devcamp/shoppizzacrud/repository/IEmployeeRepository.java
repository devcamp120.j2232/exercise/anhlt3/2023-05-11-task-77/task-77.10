package com.devcamp.shoppizzacrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shoppizzacrud.model.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee, Integer> {
    
}
